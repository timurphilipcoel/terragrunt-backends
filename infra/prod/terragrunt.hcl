include "root" {
  path = find_in_parent_folders()
}

locals {
  bucket_name = "ansd98h981asdasdaasd12sdhnb2980oh89021h980"
  account_id  = "186029991925"
  aws_region  = "eu-central-1"
}

remote_state {
  backend = "s3"
  config = {
    bucket  = local.bucket_name
    key     = "${path_relative_to_include()}/terraform.tfstate"
    region  = local.aws_region,
    encrypt = true
  }
}

iam_role = "arn:aws:iam::${local.account_id}:role/ProdAssumeRole"
